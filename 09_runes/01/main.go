package main

import "fmt"

func main() {
	i := uint64(1<<32 - 1)
	j := uint32(i<<32 - 1)
	k := int32(i<<31 - 1)
	r := rune('r')  // rune == int32
	s := int32(115) // int32

	fmt.Printf("%T %b %d\n", i, i, i)
	fmt.Printf("%T %b %d\n", j, j, j)
	fmt.Printf("%T %b %d\n", k, k, k)
	fmt.Printf("%T %b %d %q\n", r, r, r, r)
	fmt.Printf("%T %b %d %q\n", s, s, s, s)

	fmt.Println([]byte("world")) // english
	fmt.Println([]byte("世界"))    // chinese
	fmt.Println([]byte("世界"))    // japanese
	fmt.Println([]byte("세계"))    // korean

	x := '세' // threee

	fmt.Println(x)
	fmt.Println([]byte(string(x)))
	fmt.Printf("%b\n", x)

	for _, y := range []byte(string(x)) {
		fmt.Printf("%b", y)
	}
	fmt.Println()
}
