package main

import "fmt"

const start = 50
const upto = 10000

func main() {
	for i := start; i <= upto; i++ {
		fmt.Printf("%04v - %v - %v\n", i, string(i), []byte(string(i)))
	}
}
