package main

import "fmt"

func main() {
	x := 42
	fmt.Println(x)

	// here be closure
	{
		y := 32
		fmt.Println(y)
	}

	wut := func() string {
		fmt.Println("I be foo (a function literal)")
		return "something"
	}

	fmt.Println(wut())

	fn := hazClosure()
	fn()
	callme(fn)

	fn2 := hazClosure()
	fn2()
	callme(fn2)
}

func hazClosure() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func callme(fn func() int) {
	fmt.Println(fn())
	fmt.Println(fn())
}
