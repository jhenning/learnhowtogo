package main

import "fmt"

const untypedString = "untyped hello"
const typedString string = "typed hello"

type someStringType string

func main() {

	untyped := untypedString
	typed := typedString

	var myUntypedStringTypeString someStringType = untypedString

	// cannot: typedString has type != someStringType
	// var myTypedStringTypeString someStringType = typedString
	// with type conversion can:
	myConvertedStringTypeString := someStringType(typedString)

	fmt.Println(untyped)
	fmt.Println(typed)
	fmt.Println(myUntypedStringTypeString)
	fmt.Printf("%v %T\n", myConvertedStringTypeString, myConvertedStringTypeString)
}
