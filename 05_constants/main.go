package main

import (
	"fmt"
	"math"
)

// byte sizes
const (
	_  = iota
	KB = 1 << (iota * 10) // 2^10
	MB = 1 << (iota * 20) // 2^20
)

func main() {
	fmt.Printf("%d\n", KB)
	fmt.Printf("%b\n", KB)
	fmt.Printf("%v\n", math.Pow(2, 10))
}
