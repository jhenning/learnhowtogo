package main

import "fmt"

func main() {
	fmt.Printf("%v\n", !(false && false))                                       // true
	fmt.Printf("%v\n", (true && false) || (false && true) || !(false && false)) // true!
}
