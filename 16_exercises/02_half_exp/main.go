package main

import "fmt"

func main() {
	result, remainder := (func(num int) (int, int) {
		return num / 2, num % 2
	})(9)
	fmt.Println(result, remainder)
}
