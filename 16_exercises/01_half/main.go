package main

import "fmt"

func main() {
	result, isEven := half(5)
	fmt.Println(result, isEven)
}

func half(num int) (float64, bool) {
	return float64(num) / 2.0, num%2 == 0
}
