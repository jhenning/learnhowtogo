package utils

import "fmt"

// Say something
func Say(something string) {
	doSay("someone said \"" + something + "\"")
}

func doSay(something string) {
	fmt.Println(something)
}
