package main

import (
	"errors"
	"log"
)

func main() {
	if i, err := hasError(); err != nil {
		log.Fatal(err)
	} else {
		log.Println(i)
	}
}

func hasError() (int, error) {
	return 0, errors.New("Something smells funny")
}
