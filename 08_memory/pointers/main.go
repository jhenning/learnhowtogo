package main

import "fmt"

func main() {

	a := 32
	b := &a

	fmt.Println(a)
	fmt.Println(*b)

	incr(&a)
	incr(b)

	fmt.Println(a)
	fmt.Println(*b)
}

func incr(ptr *int) {
	*ptr++
}
