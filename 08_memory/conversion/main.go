package main

import (
	"fmt"
)

const metersPerLap = 50

func main() {
	var laps int
	fmt.Print("Laps swam: ")
	fmt.Scan(&laps)
	fmt.Printf("%v meters\n", laps*metersPerLap)
}
