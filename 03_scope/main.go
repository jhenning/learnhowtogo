package main

import (
	"fmt"
)

var x = 42
var y = 32

func debug(val interface{}) {
	fmt.Printf("%#v %T\n", val, val)
}

func main() {
	debug(x)
	debug(y)
	foo()
	local("18")
	debug(foo)
}

func local(y string) {
	x := 7
	debug(x)
	debug(x)
}

func foo() {
	debug(x)
}
