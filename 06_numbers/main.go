package main

import (
	"fmt"
	"math"
)

const myNum = 2
const myString = "some"

func main() {
	i := 2.0
	fmt.Println(math.Sqrt(i))
	fmt.Println(math.Sqrt(2))
	fmt.Println(math.Sqrt(myNum))
	fmt.Printf("%T\n", myNum)
	fmt.Printf("%T\n", myString)
}
