package main

import (
	"fmt"
	"log"
)

func main() {
	var num1, num2 float64
	for {
		fmt.Print("Please enter 2 numbers: ")
		if _, err := fmt.Scan(&num1, &num2); err != nil {
			log.Fatal(err)
		}
		if num1 <= num2 {
			fmt.Println("First number must be greater than second number")
			continue
		}
		fmt.Printf("Result is %.2f\n", num1/num2)
		break
	}
}
