package main

import (
	"fmt"
	"log"
)

func main() {
	var max int
	fmt.Print("Print even numbers up to: ")
	if _, err := fmt.Scan(&max); err != nil {
		log.Fatal(err)
	}
	for i := 0; i < max; i += 2 {
		fmt.Println(i)
	}
}
