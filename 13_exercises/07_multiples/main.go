package main

import "fmt"

func main() {
	fmt.Println(sumMultiples(10))
	fmt.Println(sumMultiples(1000))
}

func sumMultiples(max int) int {
	sum := 0
	for i := 0; i < max; i++ {
		switch {
		case i%3 == 0, i%5 == 0:
			sum += i
		}
	}
	return sum
}
