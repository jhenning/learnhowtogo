package main

import "fmt"

func main() {
	var fname, lname string
	fmt.Print("Enter your name: ")
	fmt.Scan(&fname, &lname)
	fmt.Printf("Hello %s %s!\n", fname, lname)
}
